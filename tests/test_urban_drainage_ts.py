#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Example test file."""

import unittest
import datetime

from examplepydebuggingandtesting import example


class TestUrbanDrainageTS(unittest.TestCase):
    """ base test class. """

    def setUp(self):
        """set up test variables."""
        time_time = [datetime.datetime.strptime('2018-01-01 12:00:00', '%Y-%m-%d %H:%M:%S'),
                     datetime.datetime.strptime('2018-01-01 12:05:00', '%Y-%m-%d %H:%M:%S'),
                     datetime.datetime.strptime('2018-01-01 12:10:00', '%Y-%m-%d %H:%M:%S'),
                     datetime.datetime.strptime('2018-01-01 12:15:00', '%Y-%m-%d %H:%M:%S'),
                     datetime.datetime.strptime('2018-01-01 12:20:00', '%Y-%m-%d %H:%M:%S'),
                     datetime.datetime.strptime('2018-01-01 12:25:00', '%Y-%m-%d %H:%M:%S'),
                     datetime.datetime.strptime('2018-01-01 12:30:00', '%Y-%m-%d %H:%M:%S'),
                     datetime.datetime.strptime('2018-01-01 12:35:00', '%Y-%m-%d %H:%M:%S'),
                     datetime.datetime.strptime('2018-01-01 12:40:00', '%Y-%m-%d %H:%M:%S')]

        value_value = [6.789, 4.567, 9.09, 6.70, 8.70, 3.4, 2.3, 4.56, 2.78]

        self.test_case = example.UrbanDrainageTS(time_time, value_value)

        # def tearDown(self):
        #     """tear down"""


class TestInit(TestUrbanDrainageTS):
    """ test initialization. """

    def test_initial_length(self):
        """ method docstring. """
        self.assertIsNotNone(self.test_case.time)
        self.assertIsNotNone(self.test_case.value)


class TestSampleEntropy(TestUrbanDrainageTS):
    """ test sample entropy. """

    def test_sample_entropy(self):
        """ method docstring. """

    def test_max_dist(self):
        """ method docstring. """
