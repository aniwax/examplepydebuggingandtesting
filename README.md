# Debugging and testing in Python

A collection of debugging and testing possibilities for the UWE team. 

See example.ipynb for more information.

## Acknowledgments

* Hat tip to anyone whose code was used