#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Example module."""

import datetime
import pandas
import pandas.api.types
import numpy


class UrbanDrainageTS(pandas.DataFrame):
    """ class for handling single measurement time series. """

    def __init__(self, time, value):
        """description of the classes initiation."""
        if isinstance(time, list):
            time = pandas.Series(time)
        if isinstance(value, list):
            value = pandas.Series(value)
        d_f = pandas.DataFrame({'time': time, 'value': value})
        assert pandas.api.types.is_datetime64_any_dtype(time), "time is not a datetime: %r" % time
        assert pandas.api.types.is_float_dtype(value), "value is not a float: %r" % value
        super().__init__(d_f)

    def sample_entropy(self, m_m=2):
        """compute sample entropy

         negative logarithm of the probability that if two sets of simultaneous data points of
         length m have distance < r then two sets of simultaneous data points of length m + 1
         m+1 also have distance < r
         """
        u_u = self.value
        r_r = 0.2 * numpy.std(self.value)

        def _maxdist(x_i, x_j):
            """compute sample entropy."""
            result = max([abs(ua_ua - va_va) for ua_ua, va_va in zip(x_i, x_j)])
            return result

        def _phi(m_m):
            """compute sample entropy."""
            x_x = [[u_u[j_j] for j_j in range(i_i, i_i + m_m - 1 + 1)]
                   for i_i in range(n_n - m_m + 1)]
            c_c = 1. * numpy.array([len([1 for j_j in range(len(x_x))
                                         if i_i != j_j and _maxdist(x_x[i_i], x_x[j_j]) <= r_r])
                                    for i_i in range(len(x_x))])
            return sum(c_c)

        n_n = len(u_u)
        return -numpy.log(_phi(m_m + 1) / _phi(m_m))

if __name__ == '__main__':


    TIME_TIME = [datetime.datetime.strptime('2018-01-01 12:00:00', '%Y-%m-%d %H:%M:%S'),
                 datetime.datetime.strptime('2018-01-01 12:05:00', '%Y-%m-%d %H:%M:%S'),
                 datetime.datetime.strptime('2018-01-01 12:10:00', '%Y-%m-%d %H:%M:%S')]

    VALUE_VALUE = [6.789, 4.567, 9.09]

    TMP_TS = UrbanDrainageTS(TIME_TIME, VALUE_VALUE)

    TMP_TS.sample_entropy(m_m=1)
