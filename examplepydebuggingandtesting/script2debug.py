# Some class defenition
class Calculator():
    
    def __init__(self,num):
        self.num = num
        return
    
    def square(self):
        return self.num**2
    
    def divide(self, divisor):
        self.count = -1
        num = self.num
        while num > 0 :
            self.count += 1
            num -= divisor
        return self.count
    
    def betafunc(self,*args):
        self.args = args
        return sum(self.args)


# Some example code that will bis executed if file is used as __main__
if __name__ == "__main__":
    
    print("This is a simple example")
    variable1 = 1
    variable2 = "1"

    print("Try one plus one.")
    
    two = variable1 + variable2
